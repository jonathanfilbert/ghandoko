# CORONAHUB
A one stop hub to connect logistics donators and hospitals in times of the spread of COVID-19

## How to run in your local machine
#### Clone this repository
```
git clone https://gitlab.com/coronahub/ghandoko.git
```

#### Make your own virtual environment in your local
```
python -m venv env
```

#### Activate your local environment
```
cd env/Scripts/
activate.bat
```
or in Mac/Linux
```
cd env/Scripts/
./activate
```

#### Install the dependencies in your virtual environment
```
pip install -r requirements.txt
```

#### Migrate first
```
python manage.py migrate
```

#### Run the web in local server
```
python manage.py runserver
```

## Contribution Code of Conduct
#### View the overall design and design system [here](https://www.figma.com/file/Ia30ItdKMZPBHwbbVH6Atg/coronahub?node-id=0%3A1)
#### Post your issues
#### Fork and implement your features
#### Pull Request!