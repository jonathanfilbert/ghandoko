from django.urls import path, include
from . import views

urlpatterns = [
    path('dashboard/', views.index, name='dashboard'),
    path('dashboard/donation', views.donation, name='donation'),
    path('dashboard/request', views.request_item, name='request'),
]