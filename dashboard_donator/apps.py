from django.apps import AppConfig


class DashboardDonatorConfig(AppConfig):
    name = 'dashboard_donator'
