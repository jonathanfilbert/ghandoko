from django.urls import path, include
from . import views

urlpatterns = [
    path('', views.searchindex, name='searchindex'),
    path('[id]', views.search, name='search'),
    path('detail', views.detail, name='detail'),
]