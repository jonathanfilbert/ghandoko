from django.shortcuts import render

# Create your views here.
def searchindex(request):
	return render(request, 'searchindex.html')


def search(request):
	return render(request, 'search.html')


def detail(request):
	return render(request, 'detail.html')